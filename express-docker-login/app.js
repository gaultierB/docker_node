var express = require("express");
var path = require("path");
var userHelper = require("./helpers/userHelper");
var usersRouter = require("./routes/users");
var app = express();

app.use(express.json());
app.use(express.urlencoded());

const url = "mongodb://gogopower.space:27017";
const dbName = "docker";
let db;
const MongoClient = require("mongodb").MongoClient;
MongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {
  console.log("Connected successfully to server");
  db = client.db(dbName);
});

app.all("/*", function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Content-Type, Authorization, Content-Length, X-Requested-With"
  );
  next();
});

app.get("/getUser/:id", (req, res) => {
  const { id } = req.params.id;
  let user;
  db.collection("users").findOne({ id: id }, function (err, result) {
    if (err) throw err;
    console.log(result);
    res.status(200).json(user);
  });
});

app.post("/add_user", (req, res) => {
  let user = req.body;
  console.log(req.body);

  db.collection("users")
    .insertOne(user)
    .then(() => {
      res.setHeader("Content-Type", "text/html");
      res.write("<p>test</p>");
      res.end();
      //  res.status(200).json({ succes: true });
    })
    .catch((err) => {
      res.status(400).json({ succes: false, error: err });
    });
});

app.post("/login", (req, res) => {
  let data = req.body; // {email // password}

  userHelper
    .getUser({ email: data.email })
    .then((user) => {
      return {
        pass: security.testPassword(data.password, user.password),
        user: user,
      };
    })
    .catch((err) => {
      res.send("Invalid credits");
    });
});

app.get("/getall", (req, res) => {
  db.collection("users").find({}).toArray(function(err, result) {
    if (err) throw err;
    console.log(result);
    res.send(result)
  });
});

// middleware
app.use("/users", usersRouter);

var listener = app.listen(8080, function () {
  console.log("Listening on port " + listener.address().port);
});
